﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrderManager : MonoBehaviour
{

    public Ingredient.Type[] ingredientTypes;
    [Header("Models")]
    public ModelOrder modelOrder;
    public ModelPizza modelPizza;

    public int numTables = 3;
    public int maxNumOrders;
    public Transform orderOrigin;
    public Vector3 orderOffset;

    public GameObject prefabOrder;
    //Current Orders
    public List<Order> orders;
    public bool[] orderSlots;
    public int orderCount = 0;

    public AnimationCurve ingredientCurve;


    private ScoreManager scoreManager;
    private static OrderManager instance;

    public void Awake()
    {
        if (instance != null)
            throw new UnityException("Multiple order managers found");
        instance = this;
    }

    public static OrderManager getInstance()
    {
        return instance;
    }

    public void Start()
    {
        orders = new List<Order>();
        orderSlots = new bool[maxNumOrders];
        scoreManager = ScoreManager.getInstance();

        modelOrder.ingredientTypes = ingredientTypes;
        modelPizza.ingredientTypes = ingredientTypes;

    }

    public void Update()
    {
        //for testing
        if (Input.GetKeyDown(KeyCode.Alpha1))
            newOrder(GameManager.getInstance().level);
    }

    public void clear()
    {
        for (int i = orders.Count - 1; i > -1; i--)
        {
            Destroy(orders[i].gameObject, 1f);
            StartCoroutine(setSlotFree(1f, orders[i].slot));
            orders[i].setFailed();
            orders.RemoveAt(i);
            orderCount = 0;
        }

    }

    //difficulty is how many rows will be generated
    public int newOrder(int level)
    {
        int orderIndex = -1;
        for (int i = 0; i < orderSlots.Length; i++)
        {
            if (!orderSlots[i])
            {
                orderIndex = i;
                orderSlots[i] = true;
                break;
            }
        }
        if (orderIndex == -1)
        {
            Debug.LogWarning("Max number of orders reached");
            return -1;
        }

        GameObject g = Instantiate(prefabOrder, orderOrigin.transform.position + orderOffset * orderIndex, Quaternion.identity) as GameObject;
        Order o = g.GetComponent<Order>();

        int[] ingredientCount = new int[ingredientTypes.Length];

        int numIngredient = (int)ingredientCurve.Evaluate(level) - Random.Range(0,2);
        int j = 0;
        while(j < numIngredient){
            int index = Random.Range(0,ingredientTypes.Length);
            ingredientCount[index]++;
            j++;
        }

        o.set(Random.Range(1, numTables+1), ingredientCount);
        o.slot = orderIndex;
        o.id = orderCount++;

        orders.Add(o);
        return orderIndex;
    }

    public bool delivered(Pizza pizza, int table_num)
    {
        int difficulty = 1;

        if (!pizza.cooked || pizza.burnt) return false;

        bool found = false;
        int i = 0;
        for (; i < orders.Count; i++)
        {
            bool correctIngredients = true;
            for (int j = 0; j < orders[i].ingredientCount.Length; j++)
            {
                if (pizza.ingredientCount[j] != orders[i].ingredientCount[j])
                {
                    correctIngredients = false;
                    break;
                }

            }
            if (!correctIngredients || orders[i].tableNumber != table_num)
                continue;

            found = true;

            difficulty = orders[i].difficulty;

            StartCoroutine(setSlotFree(1.5f, orders[i].slot));
            orders[i].setDone();
            Destroy(orders[i].gameObject, 1f);
            orders.RemoveAt(i);
            orderCount--;

            break;
        }

        if (found)
        {
            scoreManager.score += 1;
        }
        else
        {

        }

        return found;

    }

    IEnumerator setSlotFree(float seconds, int index)
    {
        yield return new WaitForSeconds(seconds);
        Debug.Log("falsing " + index);
        orderSlots[index] = false;

    }


}
