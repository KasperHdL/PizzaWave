﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeaderboardManager : MonoBehaviour
{
    public int num_highscores = 10;
    public Text highscore_text;

    private string highscore_start_text;

    private string[] leader_names;
    private int[] leader_scores;

    private static string name_string = "hi_name";
    private static string score_string = "hi_score";

    private static LeaderboardManager instance;

    public void Awake()
    {
        if (instance != null)
            throw new UnityException("Multiple LeaderboardManagers found");

        instance = this;
    }

    void Start()
    {
        highscore_start_text = highscore_text.text;
        LoadScores();
        UpdateUIText();
    }

    public int CheckHighscore(int score)
    {
        int highscore_index = -1;
        for (int i = 0; i < num_highscores; i++)
        {
            if (score > leader_scores[i])
            {
                highscore_index = i;
                break;
            }
        }
        return highscore_index;
    }

    public void AddNewHighscore(int score, string name, int index)
    {
        for (int i = num_highscores; i >= index; i--)
        {
            if (i != index)
            {
                leader_scores[i] = leader_scores[i - 1];
                leader_names[i] = leader_names[i - 1];
            }
            else
            {
                leader_scores[i] = score;
                leader_names[i] = name;
            }
        }
    }

    private void SaveScores()
    {
        for (int i = 0; i < num_highscores; i++)
        {
            PlayerPrefs.SetString(name_string + i, leader_names[i]);
            PlayerPrefs.SetInt(score_string + i, leader_scores[i]);
        }
    }

    private void LoadScores()
    {
        for (int i = 0; i < num_highscores; i++)
        {
            leader_names[i] = PlayerPrefs.GetString(name_string + i, "name");
            leader_scores[i] = PlayerPrefs.GetInt(score_string + i, 0);
        }
    }

    private void UpdateUIText()
    {
        for (int i = 0; i < num_highscores; i++)
        {
            highscore_text.text = highscore_start_text;
            // if (leader_names[i] != "name")
            {
                highscore_text.text += leader_names[i] + "\t\t-\t" + leader_scores[i];
            }
        }
    }

    public static LeaderboardManager getInstance()
    {
        return instance;
    }
}
